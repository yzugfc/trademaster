#SingleInstance force
#NoEnv

; CHotkeyHandler controls ALL hotkeys. Users call this class to create a new hotkey guicontrol, and it instantiates a CHotkeyControl class for each one.
; Flow of control for adding and binding of a hotkey:
; 1) User selects the "Select Binding" option from an instance of the CHotkeyControl class
; 2) The CHotkeyControl instance calls the CInputDetector class and passes it a callback to itself.
; 3) The CInputDetector class presents a dialog instructing the user to pick a key.
;    Once the user selects a hotkey, CInputDetector fires the callback to the CHotkeyControl instance.
; 4) The CHotkeyControl instance then makes a call to CHotkeyHandler to ask it to bind this hotkey.
;    If the key is already bound in another CHotkeyControl instance, CHotkeyHandler presents a dialog saying that the hotkey conflicts with another...
;    ... then returns a value to the CHotkeyControl instance to instruct it to not accept that binding.
Class CHotkeyHandler {
	_BindMode := 0
	; Hotkey lookup arrays. Each hotkey has a NAME and a BINDSTRING (eg "^+a")
	_HotkeyObjects := {}		; All the instantiated Hotkey objects. name -> object
	_HotkeyCallbacks := {}		; The (user) callbacks for all the hotkeys. name -> callback
	_HotkeyBindings := {}		; The parameters used for the hotkey command. name -> bindstring
	_BoundKeys := {}			; Currently bound hotkeys. Used to detect attempt to bind same hotkey twice. NOTE: ~ is filtered out, as it does not affect uniqueness. bindstring -> name
	_HeldHotkeys := {}			; A list of Hotkeys currently in the Down state (Used for repeat suppression), name -> nothing
	
	; Public Methods ------------------------------------------------------------------------
	__New(callback := 0){
		if (callback != 0 && !IsObject(callback))
			callback := Func(callback)
		this._callback := callback
		this._InputDetector := new this.CInputDetector(this)
	}
	
	; Add a Hotkey GuiControl to a script
	AddHotkey(name, callback, options){
		if (!IsObject(callback))
			callback := Func(callback)
		this._HotkeyCallbacks[name] := callback
		this._HotkeyObjects[name] := new this.CHotkeyControl(this, name, options)
		return this._HotkeyObjects[name]
	}

	; Private Methods -------------------------------------------------------------------------
	
	; Request to enter Bind Mode
	RequestBindMode(name, callback){
		if (this._BindMode){
			return 0
		} else {
			this._BindMode := 1
			this.DisableHotkeys()
			this._InputDetector.SelectBinding(callback)
			return 1
		}
	}
	
	; After we enter Bind Mode and a binding is chosen, this gets called to decide whether or not to accept the binding
	RequestBinding(name, hk){
		StringReplace, hktmp, hk, ~
		if (ObjHasKey(this._BoundKeys, hktmp) && this._BoundKeys[hktmp] != name){
			; Duplicate hotkey
			SplashTextOn, 300, 50, Bind  Mode, % "This key combination is already bound to the following hotkey: " this._BoundKeys[hktmp]
			Sleep 2000
			SplashTextOff
			; Pass false - binding not allowed
			return 0
		}
		OutputDebug % name " Hotkey Changed to " hk
		return 1
	}
	
	; A hotkey finished detecting a user binding (Bind Mode ended), or one of the options (eg Clear, change mode) was used
	_HotkeyChanged(name, hk){
		if (hk = ""){
			this.DisableHotkey(name)
		}
		this._RegisterBinding(name, hk)
		
		if (this._BindMode){
			; If in Bind Mode, A valid key was chosen, so end Bind Mode.
			this._BindMode := 0
			this.EnableHotkeys()
		} else {
			; Not in Bind Mode - option was chosen, or value was set by external source (eg load from INI file)
			this.EnableHotkey(name, hk)
		}
		if (this._callback !=0){
			this._callback.call(name, hk)
		}
	}
	
	; Registers a binding with the hotkey handler
	_RegisterBinding(name, hk){
		if (hk = ""){
			; Update arrays
			this._BoundKeys.Delete(this._HotkeyBindings[name])
			this._HotkeyBindings.Delete(name)
		} else {
			; Update arrays to register what we bound
			StringReplace, hktmp, hk, ~
			this._HotkeyBindings[name] := hk
			this._BoundKeys[hktmp] := name
		}
	}
	
	; Binds a given hotkey GuiControl to a bindstring
	EnableHotkey(name, hk){
		; If the guicontrol is already bound, remove the existing binding
		if (ObjHasKey(this._HotkeyBindings, name) && this._HotkeyBindings[name] != hk){
			this.DisableHotkey(name)
		}
		; Make the new binding
		if (hk != ""){
			try {
				fn := this._HotkeyEvent.Bind(this, name, 1)
				hotkey, % "$" hk, % fn, On
				if (this._HotkeyObjects[name]._type = 0){
					fn := this._HotkeyEvent.Bind(this, name, 0)
					hotkey, % "$" hk " up", % fn, On
				}
				this._RegisterBinding(name, hk)
			} catch {
				OutputDebug % "Enable Hotkey for " name " failed! " - hk
			}
		}
	}
	
	; Disables (but does not delete) a binding for a guicontrol
	DisableHotkey(name){
		hk := this._HotkeyBindings[name]
		try {
			Hotkey, % "$" hk, Off
			Hotkey, % "$" hk " up", Off
		} catch {
			OutputDebug % "Disable hotkey for " name " failed - " hk
		}
	}
	
	; Enable all hotkeys
	EnableHotkeys(){
		this._HeldHotkeys := {}
		for name, hk in this._HotkeyBindings {
			this.EnableHotkey(name, hk)
		}
	}

	; Disable all hotkeys
	DisableHotkeys(){
		this._HeldHotkeys := {}
		for name, hk in this._HotkeyBindings {
			this.DisableHotkey(name)
		}
	}
	
	; Called whenever a hotkey goes down or up.
	_HotkeyEvent(name, event){
		if (event){
			if (this._HotkeyObjects[name]._norepeat && ObjHasKey(this._HeldHotkeys, name))
				return
			else 
				this._HeldHotkeys[name] := 1
		} else {
			this._HeldHotkeys.Delete(name)
		}
		OutputDebug % "Hotkey " name " - " event
		this._HotkeyCallbacks[name].(event)
		; Simulate up events for joystick buttons
		if (event = 1 && this._HotkeyObjects[name]._type = 1){
			StringReplace, str, % this._HotkeyBindings[name], ~
			while(GetKeyState(str)){
				Sleep 10
			}
			this._HotkeyEvent(name, 0)
		}
	}
	
	; CHotkeyControl handles the GUI for an individual Hotkey GuiControl.
	; It facilitates selection of hotkey options (wild, passthrough, repeat suppression etc) and displaying of the selected hotkey in a human-readable format.
	Class CHotkeyControl {
		; Internal vars describing the bindstring
		_value := ""		; The bindstring of the hotkey (eg ~*^!a). The getter for .value returns this
		_hotkey := ""		; The bindstring without any modes (eg ^!a)
		_wild := 0			; Whether Wild (*) mode is on
		_passthrough := 1	; Whether Passthrough (~) mode is on
		_norepeat := 0		; Whether or not to suppress repeat down events
		_type := 0			; 0 = keyboard / mouse, 1 = joystick button
		; Other internal vars
		_DefaultBanner := "Drop down list to select a binding"
		_modifiers := {"^": "Ctrl", "+": "Shift", "!": "Alt", "#": "Win"}
		_modes := {"~": 1, "*": 1}
		_OptionMap := {Select: 1, Wild: 2, Passthrough: 3, Suppress: 4, Clear: 5}
		; Constructor.
		; Params:
		; handler: The Hotkey Handler class. Will call various methods of this class to eg request a binding, set a hotkey
		; name: The (unique) name assigned to this hotkey
		; options: The AHK GuiControl options to apply to the ComboBox (eg "w300")
		__New(handler, name, options){
			this._name := name
			this._handler := handler
			
			Gui, Add, ComboBox, % "hwndhwnd " options
			this.hwnd := hwnd
			this._hEdit := DllCall("GetWindow","PTR",this.hwnd,"Uint",5) ;GW_CHILD = 5
			
			fn := this.OptionSelected.Bind(this)
			GuiControl +g, % this.hwnd, % fn
			
			this.BuildOptions()
			this.SetCueBanner()
		}
		
		; Setters and getters re-route .value to ._value
		; Set of value triggers update of GUI, but does not request setting of hotkey
		value[]{
			get {
				return this._value
			}
			
			set {
				this.SetValue(value)
			}
		}
		
		; Set the entire state of the hotkey - Modifier, Keys *and* Modes.
		; This code is currently only to support programatically setting a hotkey by .value (eg when loading binding state from an INI file)
		SetValue(value){
			max := StrLen(value)
			str := ""
			this._wild := this._passthrough := 0
			loop % max {
				i := A_Index
				c := SubStr(value, A_Index, 1)
				if (ObjHasKey(this._modes, c)){
					if (c = "*")
						this._wild := 1
					if (c = "~")
						this._passthrough := 1
					max--
					str .= c
				} else {
					break
				}
			}
			hk := SubStr(value, i, max)
			value := str hk
			if (this._handler.RequestBinding(this._name, value)){
				this._UpdateValue(hk, value)
				this._handler.EnableHotkey(this._name, value)
			}
			; else ??
		}
		
		; The Binding (That is, only the Modifiers and EndKeys) Changed
		; Modes (eg ~, *) are not set by this routine
		; This is so that if the user enables passthrough, and then changes binding, passthrough is not reset
		; Requests permission from the Handler to accept this bindstring (It may reply no if another Hotkey control is already bound to that bindstring)
		ChangeHotkey(hk){
			value := this.BuildValue(hk)
			; Request Binding from hotkey handler
			if (!this._handler.RequestBinding(this._name, value)){
				; Binding rejected - end bind mode, pass original value
				this._handler._HotkeyChanged(this._name, this._value)
				return
			}
			this._UpdateValue(hk, value)
			
			; End Bind Mode and pass new value
			this._handler._HotkeyChanged(this._name, value)
		}
		
		; Updates the state of the hotkey
		_UpdateValue(hk, value){
			this._hotkey := hk
			this._value := value
			if (InStr(hk, "Joy")){
				this._type := 1
			} else {
				this._type := 0
			}
			this.HumanReadable := this.BuildHumanReadable()
			this.SetCueBanner()
			this.BuildOptions()
		}

		; An option was selected from the list
		OptionSelected(){
			; Find index of dropdown list. Will be really big number if key was typed
			SendMessage 0x147, 0, 0,, % "ahk_id " this.hwnd  ; CB_GETCURSEL
			o := ErrorLevel
			GuiControl, Choose, % this.hwnd, 0
			if (o < 100){
				o++
				; Some options may be filtered, so look up actual option ID from _CurrentOptionMap
				o := this._CurrentOptionMap[o]
				; Option selected from list
				if (o = 1){
					this._handler.RequestBindMode(this._name, this.ChangeHotkey.Bind(this))
					return
				} else if (o = this._OptionMap["Wild"]){
					this._wild := !this._wild
				} else if (o = this._OptionMap["Passthrough"]){
					this._passthrough := !this._passthrough
				} else if (o = this._OptionMap["Suppress"]){
					this._norepeat := !this._norepeat
				} else if (o = this._OptionMap["Clear"]){
					this._hotkey := ""
				} else {
					; not one of the options from the list, user must have typed in box
					return
				}
				this.ChangeHotkey(this._hotkey)
			}
		}

		; Builds the list of options in the DropDownList
		; Some items may be filtered, so a _CurrentOptionMap lookup table is created
		BuildOptions(){
			this._CurrentOptionMap := [this._OptionMap["Select"]]
			str := "|Select Binding"
			if (this._type = 0){
				; Joystick buttons do not have these options
				str .= "|Wild: " (this._wild ? "On" : "Off") 
				this._CurrentOptionMap.push(this._OptionMap["Wild"])
				str .= "|Passthrough: " (this._passthrough ? "On" : "Off")
				this._CurrentOptionMap.push(this._OptionMap["Passthrough"])
				str .= "|Repeat Suppression: " (this._norepeat ? "On" : "Off")
				this._CurrentOptionMap.push(this._OptionMap["Suppress"])
			}
			str .= "|Clear Binding"
			this._CurrentOptionMap.push(this._OptionMap["Clear"])
			GuiControl, , % this.hwnd, % str
		}
		
		; Builds an AHK hotkey string (eg "~*^C") from .hotkey and ._wild/._passthrough etc
		BuildValue(hk){
			str := ""
			if (hk != ""){
				if (this._wild)
					str .= "*"
				if (this._passthrough)
					str .= "~"
				str .= hk
			}
			return str
		}
		
		; Build Human-Readable string for a hotkey
		BuildHumanReadable(){
			if (this._hotkey = "")
				return ""
			str := ""
			prefix := ""
			if (this._wild)
				prefix .= "W"
			if (this._passthrough)
				prefix .= "P"
			
			if (prefix)
				str .= "(" prefix ") "
			
			max := StrLen(this._hotkey)
			loop % max {
				i := A_Index
				c := SubStr(this._hotkey, i, 1)
				if (ObjHasKey(this._modifiers, c)){
					str .= this._modifiers[c] " + "
					max--
				} else {
					break
				}
			}
			str .= SubStr(this._hotkey, i, max)
			return str
		}
		
		; Sets the "Cue Banner" for the ComboBox
		SetCueBanner(){
			static EM_SETCUEBANNER:=0x1501
			if (this._hotkey = "")
				Text := this._DefaultBanner
			else
				Text := this.BuildHumanReadable()
			DllCall("User32.dll\SendMessageW", "Ptr", this._hEdit, "Uint", EM_SETCUEBANNER, "Ptr", True, "WStr", text)
			return this
		}
		
	}
	
	; Handles Bind Mode
	Class CInputDetector {
		DebugMode := 1 ; 0 = Block all, 1 = Dont block LMB/RMB, 2 = Don't block any
		_StartBindMode := 0
		_Modifiers := ({91: {s: "#", v: "<"},92: {s: "#", v: ">"}
		,160: {s: "+", v: "<"},161: {s: "+", v: ">"}
		,162: {s: "^", v: "<"},163: {s: "^", v: ">"}
		,164: {s: "!", v: "<"},165: {s: "!", v: ">"}})

		__New(handler){
			this._handler := handler
		}
		
		; Public method that gets called when a user wishes to select a hotkey.
		; Callback will be called whent the user finishes selecting the hotkey.
		SelectBinding(callback){
			this._callback := callback
			this.SetHotkeyState(1)
		}
		
		; Turns on or off the binding detection hotkeys.
		; When turning off, pass the bindstring that the user selected as the 2nd param
		SetHotkeyState(state, binding := 0){
			static pfx := "$*"
			static current_state := 0
			static updown := [{e: 1, s: ""}, {e: 0, s: " up"}]
			onoff := state ? "On" : "Off"
			if (state = current_state)
				return
			if (state){
				SplashTextOn, 300, 30, Bind  Mode, Press a key combination to bind
				; Set flag to tell ProcessInput we want to initialize Bind Mode
				this._StartBindMode := 1
			} else {
				SplashTextOff
			}
			; Cycle through all keys / mouse buttons
			Loop 256 {
				; Get the key name
				i := A_Index
				code := Format("{:x}", A_Index)
				n := GetKeyName("vk" code)
				if (n = "")
					continue
				; Down event, then Up event
				Loop 2 {
					blk := this.DebugMode = 2 || (this.DebugMode = 1 && i <= 2) ? "~" : ""

					fn := this.ProcessInput.Bind(this, {type: 0, keyname: n, event: updown[A_Index].e, vk: i})
					if (state)
						hotkey, % pfx blk n updown[A_Index].s, % fn
					hotkey, % pfx blk n updown[A_Index].s, % fn, % onoff
				}
			}
			; Cycle through all Joystick Buttons
			Loop 8 {
				j := A_Index
				Loop 32 {
					n := j "Joy" A_Index
					Loop 2 {
						fn := this.ProcessInput.Bind(this, {type: 1, keyname: n, event: updown[A_Index].e, vk: i})
						if (state)
								hotkey, % pfx n updown[A_Index].s, % fn
							hotkey, % pfx n updown[A_Index].s, % fn, % onoff
						}
				}
			}
			if (!state){
				; Fire callback
				this._callback.(binding)
			}
			current_state := state
		}
		
		; Whenever a key changes state, this is called.
		; Set this._StartBindMode to 1 before binding hotkeys, to tell it to reset vars
		ProcessInput(i){
			static HeldModifiers := {}, EndKey := 0, ModifierCount := 0
			
			; Look for flag that gets set after hotkeys are turned on
			if (this._StartBindMode){
				; Initialize Bind Mode
				HeldModifiers := {}
				ModifierCount := 0
				EndKey := 0
				; reset flag
				this._StartBindMode := 0
			}
			
			if (i.type){
				is_modifier := 0
			} else {
				is_modifier := ObjHasKey(this._Modifiers, i.vk)
				; filter repeats
				if (i.event && (is_modifier ? ObjHasKey(HeldModifiers, i.vk) : EndKey) )
					return
			}

			
			; Are the conditions met for end of Bind Mode? (Up event of non-modifier key)
			if ((is_modifier ? (!i.event && ModifierCount = 1) : !i.event) && (i.type ? !ModifierCount : 1) ) {
				; End Bind Mode
				this.SetHotkeyState(0, this.RenderHotkey({HeldModifiers: HeldModifiers, EndKey: EndKey}))
				return
			} else {
				; Process Key Up or Down event
				if (is_modifier){
					; modifier went up or down
					if (i.event){
						HeldModifiers[i.vk] := i
						ModifierCount++
					} else {
						HeldModifiers.Delete(i.vk)
						ModifierCount--
					}
				} else {
					; regular key went down or up
					if (i.type && ModifierCount){
						; Reject joystick button + modifier - AHK does not support this
						if (i.event)
							SoundBeep
					} else {
						; Down event of non-modifier key - set end key
						EndKey := i
					}
				}
			}
			
			; Mouse Wheel has no Up event, so simulate it to trigger it as an EndKey
			if (i.event && (i.vk = 158 || i.vk = 159)){
				i.event := 0
				this.ProcessInput(i)
			}
		}
		
		; Converts the output from ProcessInput into a standard ahk hotkey string (eg ^a)
		RenderHotkey(hk){
			if (!hk.endkey){
				for vk, obj in hk.HeldModifiers {
					return obj.Keyname
				}
			}
			str := ""
			for vk, obj in hk.HeldModifiers {
				str .= this._Modifiers[vk].s
			}
			str .= hk.Endkey.keyname
			return str
		}
	}

}

;Initializer
isPressing := false			;Key pressing to prevent overlapping messages
looping := false			;Repeating on/off
enter := false				;Defined by the Check Button from the CheckGroup
repeat := 0					;Repeat message
msgAmount := 11				;Amount of messages (TODO: Make the GUI Dynamic, able to increase with this variable)
version := "2.3.1"			;Version of the program
maxX := A_ScreenWidth		;Screen Width (For the repeating tooltip)
maxY := A_ScreenHeight - 30	;Screen Height - 30 (For the repeating tooltip)
Loop %msgAmount% {
	IniRead, Num%A_Index%, % A_ScriptName ".ini", tradeMessages, Num%A_Index%	;Reads all the messages (TODO: Make it so the default isn't "ERROR")
}
IniRead, RepeatTimer, % A_ScriptName ".ini", settings, RepeatTimer				;Reads the repeat timer
if (RepeatTimer = "" or RepeatTimer = "ERROR" or RepeatTimer is not Integer) {
	IniWrite, 125000, % A_ScriptName ".ini", settings, RepeatTimer				;Reset repeat timer
	IniRead, RepeatTimer, % A_ScriptName ".ini", settings, RepeatTimer			;Re-read the variable
}

;GUI
; Messages
Gui, Add, GroupBox, x12 y9 w480 h465, Messages
; -Text
Gui, Add, Text, x22 y39 w20 h20 , #1
Gui, Add, Text, x22 y79 w20 h20 , #2
Gui, Add, Text, x22 y119 w20 h20 , #3
Gui, Add, Text, x22 y159 w20 h20 , #4
Gui, Add, Text, x22 y199 w20 h20 , #5
Gui, Add, Text, x22 y239 w20 h20 , #6
Gui, Add, Text, x22 y279 w20 h20 , #7
Gui, Add, Text, x22 y319 w20 h20 , #8
Gui, Add, Text, x22 y359 w20 h20 , #9
Gui, Add, Text, x22 y399 w20 h20 , #10
Gui, Add, Text, x22 y439 w20 h20 , #11
; -Edits
Gui, Add, Edit, x45 y29 w437 h35 vNum1, %Num1%
Gui, Add, Edit, x45 y69 w437 h35 vNum2, %Num2%
Gui, Add, Edit, x45 y109 w437 h35 vNum3, %Num3%
Gui, Add, Edit, x45 y149 w437 h35 vNum4, %Num4%
Gui, Add, Edit, x45 y189 w437 h35 vNum5, %Num5%
Gui, Add, Edit, x45 y229 w437 h35 vNum6, %Num6%
Gui, Add, Edit, x45 y269 w437 h35 vNum7, %Num7%
Gui, Add, Edit, x45 y309 w437 h35 vNum8, %Num8%
Gui, Add, Edit, x45 y349 w437 h35 vNum9, %Num9%
Gui, Add, Edit, x45 y389 w437 h35 vNum10, %Num10%
Gui, Add, Edit, x45 y429 w437 h35 vNum11, %Num11%
; Hotkeys
hh := new CHotkeyHandler("HotkeyChanged")
hotkeys := {}
Gui, Add, GroupBox, x491 y9 w180 h465 , Hotkey
hotkeys.hk1 := hh.AddHotkey("hk1", "HK1Pressed", "x502 y29 w160")
hotkeys.hk2 := hh.AddHotkey("hk2", "HK2Pressed", "x502 y69 w160")
hotkeys.hk3 := hh.AddHotkey("hk3", "HK3Pressed", "x502 y109 w160")
hotkeys.hk4 := hh.AddHotkey("hk4", "HK4Pressed", "x502 y149 w160")
hotkeys.hk5 := hh.AddHotkey("hk5", "HK5Pressed", "x502 y189 w160")
hotkeys.hk6 := hh.AddHotkey("hk6", "HK6Pressed", "x502 y229 w160")
hotkeys.hk7 := hh.AddHotkey("hk7", "HK7Pressed", "x502 y269 w160") 
hotkeys.hK8 := hh.AddHotkey("hk8", "HK8Pressed", "x502 y309 w160")
hotkeys.hK9 := hh.AddHotkey("hk9", "HK9Pressed", "x502 y349 w160")
hotkeys.hK10 := hh.AddHotkey("hk10", "HK10Pressed", "x502 y389 w160")
hotkeys.hK11 := hh.AddHotkey("hk11", "HK11Pressed", "x502 y429 w160")
; Repeat
Gui, Add, GroupBox, x670 y9 w30 h465 , Repeat
Gui, Add, Radio, x680 y29 w18 h35 altsubmit gCheck vRadioGroup,
Gui, Add, Radio, x680 y69 w18 h35 altsubmit gCheck, 
Gui, Add, Radio, x680 y109 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y149 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y189 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y229 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y269 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y309 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y349 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y389 w18 h35 altsubmit gCheck,
Gui, Add, Radio, x680 y429 w18 h35 altsubmit gCheck,
; Bottom
Gui, Add, Text, x12 y481 , Toggle repeat
hotkeys.hK12 := hh.AddHotkey("hk12", "HK12Pressed", "x+5 y479 w130")
Gui, Add, Edit, x+5 y479 w60 h20 vRepeatTimer, %RepeatTimer%
Gui, Add, Text, x+5 y481 w20 h20 , ms
Gui, Add, Text, x+20 y481 , Auto-Enter
Gui, Add, CheckBox, x+5 y479 w60 h20 gEnter vCheckGroup,
Gui, Add, Text, x610 y499 w90 h35 , Trademaster %version%
Gui, Add, Button, x12 y509 w50 h20 Default gSave, Save
; Load and show
LoadHotkeys()
Gui, Show, x120 y106 h535 w720, Trademaster
Return

;Functions
F11::
ExitApp

GuiClose:
ExitApp

; 1st message hotkey - Disclaimer: All message hotkeys are Identical
HK1Pressed(event){
	global isPressing
	global Num1
	ifWinActive, Warframe
	{
		if (event = 1) {				;Checks if key is pressed
			if (isPressing = false) {	;Checks if the key is being pressed to prevent multiple triggers
				Send, %Num1%			;Sends the defined message for this hotkey
				isPressing := true		;Changes isPressing=true to prevent multiple triggers
				if (enter = True) {		;If the check button from CheckGroup is active enter=true
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 2nd message hotkey
HK2Pressed(event){
	global isPressing
	global Num2
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num2%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 3rd message hotkey
HK3Pressed(event){
	global isPressing
	global Num3
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num3%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 4th message hotkey
HK4Pressed(event){
	global isPressing
	global Num4
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num4%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 5th message hotkey
HK5Pressed(event){
	global isPressing
	global Num5
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num5%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 6th message hotkey
HK6Pressed(event){
	global isPressing
	global Num6
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num6%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 7th message hotkey
HK7Pressed(event){
	global isPressing
	global Num7
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num7%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 8th message hotkey
HK8Pressed(event){
	global isPressing
	global Num8
	ifWinActive, Warframe 
		{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num8%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 9th message hotkey
HK9Pressed(event){
	global isPressing
	global Num9
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num9%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 10th message hotkey
HK10Pressed(event){
	global isPressing
	global Num10
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num10%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; 11th message hotkey
HK11Pressed(event){
	global isPressing
	global Num11
	ifWinActive, Warframe 
	{
		if (event = 1) {
			if (isPressing = false) {
				Send, %Num11%
				isPressing := true
				if (enter = True) {
					Send, {Enter}
					}
				}
			} else {
			isPressing := false
		}
	}
	return
}

; Repeat timer hotkey
HK12Pressed(event){
	global isPressing
	global RepeatTimer
	global looping
	global repeat
	global maxY
	global maxX
	if (event = 1) {
		if (isPressing = false) {
			if(looping) {				;Checks if looping = true || false (Toggle)
				Repeating(false, "")	
			} else {					
				if (repeat != 0) {		;Checks if a radio from the RadioGroup is selected
					Repeating(true, "")
					return
				} else {
					MsgBox, 16, Error, No message set to repeat
					return
				}
			}
			isPressing := true
			}
		} else {
		isPressing := false
	}
	return
}

; A Hotkey changed binding - save value to ini file
HotkeyChanged(name, hk){
	global hotkeys
	IniWrite, % hk, % A_ScriptName ".ini", Hotkeys, % name
	ToolTip % "Hotkey " name " Changed binding to: " hotkeys[name].HumanReadable
	SetTimer, TT, -500
}

; Load hotkey values from INI file
LoadHotkeys(){
	global hotkeys
	for name, hk in hotkeys {
		IniRead, value, % A_ScriptName ".ini", Hotkeys, % name
		if (value != "" && value != "ERROR")
			hotkeys[name].value := value
	}
}

; Repeating Function - Repeats a message depending on boolean value
Repeating(boolean, extra) {
	global RepeatTimer
	global looping
	global repeat
	global maxY
	global maxX
	if (boolean = true) {
		looping := true						;Sets looping for toggle
		SetTimer, Repeat, %RepeatTimer%		;Timer repeats every defined time (RepeatTimer)
		ToolTip, Repeating: on `nDuration: %RepeatTimer% ms %extra%, maxX, maxY
	} else {
		looping := false					;Sets looping for toggle
		SetTimer, Repeat, Off				;Timer off
		ToolTip, Repeating: off %extra%, maxX, maxY
		SetTimer, tt, -2500
	}
}

; Save Fuction - Runs when the Save button is pressed
Save:
{
	Gui, submit, nohide
	msg := ""
	Loop %msgAmount% {
		IniWrite, % Num%A_Index%, % A_ScriptName ".ini", tradeMessages, Num%A_Index%	;Saves the messages
		numMsg := % Num%A_Index%														;Writes the current message to a var
		msg = %msg% `n`nMessage  %A_Index% : %numMsg%									;adds the default message to the overall save message
	}
	IniWrite, %RepeatTimer%, % A_ScriptName ".ini", settings, RepeatTimer				;Saves the repeat time
	MsgBox Saved! %msg%`n`nRepeat duration: %RepeatTimer% 								;Opens a message box with the save messages and the reapeat message
	return
}

; Enter Fuction - Runs when Check button from the CheckGroup is selected
Enter:
{
	gui, submit, nohide
	global enter 			;boolean var
	if (CheckGroup = 1) {	;Checks if enter is activated
		enter := true
	} else {
		enter := false
	}
}

;Check Funcion - Runs when a radio from the RadioGroup is selected
Check:
{
	gui, submit, nohide
	Loop %msgAmount% {
		if (RadioGroup = A_Index)	;Interates through the radio group until it finds the selected one
			repeat = % Num%A_Index% ;Saves the message to the repeat var
			return
	}
}

; Repeat Function - Runs when called
Repeat:
{
	global repeat
	global RepeatTimer
	global looping 
	global maxY
	global maxX
	ifWinActive, Warframe 
	{
		if (repeat != 0) {	;Checks if a radio is selected
			Send, %repeat%
			Send, {enter}
			return
		} else {			;Radio not selected
			Repeating(false, "`nError: No message set to repeat")
		return
		}
	} else {				;Warframe not the active window
		Repeating(false, "`nError: Warframe not active window")
	return
	}
}

; Tootip Function (for compatibility) - Runs when called
TT:
ToolTip
return