; Messages
Gui, Add, GroupBox, x12 y9 w480 h465, Messages
; -Text
Gui, Add, Text, x22 y39 w20 h20 , #1
Gui, Add, Text, x22 y79 w20 h20 , #2
Gui, Add, Text, x22 y119 w20 h20 , #3
Gui, Add, Text, x22 y159 w20 h20 , #4
Gui, Add, Text, x22 y199 w20 h20 , #5
Gui, Add, Text, x22 y239 w20 h20 , #6
Gui, Add, Text, x22 y279 w20 h20 , #7
Gui, Add, Text, x22 y319 w20 h20 , #8
Gui, Add, Text, x22 y359 w20 h20 , #9
Gui, Add, Text, x22 y399 w20 h20 , #10
Gui, Add, Text, x22 y439 w20 h20 , #11
; -Edits
Gui, Add, Edit, x45 y29 w437 h35 , Edit
Gui, Add, Edit, x45 y69 w437 h35 , Edit
Gui, Add, Edit, x45 y109 w437 h35 , Edit
Gui, Add, Edit, x45 y149 w437 h35 , Edit
Gui, Add, Edit, x45 y189 w437 h35 , Edit
Gui, Add, Edit, x45 y229 w437 h35 , Edit
Gui, Add, Edit, x45 y269 w437 h35 , Edit
Gui, Add, Edit, x45 y309 w437 h35 , Edit
Gui, Add, Edit, x45 y349 w437 h35 , Edit
Gui, Add, Edit, x45 y389 w437 h35 , Edit
Gui, Add, Edit, x45 y429 w437 h35 , Edit
; Hotkeys
Gui, Add, GroupBox, x491 y9 w180 h465 , Hotkey
Gui, Add, Hotkey, x502 y29 w160 h35 , 
Gui, Add, Hotkey, x502 y69 w160 h35 , 
Gui, Add, Hotkey, x502 y109 w160 h35 , 
Gui, Add, Hotkey, x502 y149 w160 h35 , 
Gui, Add, Hotkey, x502 y189 w160 h35 , 
Gui, Add, Hotkey, x502 y229 w160 h35 , 
Gui, Add, Hotkey, x502 y269 w160 h35 , 
Gui, Add, Hotkey, x502 y309 w160 h35 , 
Gui, Add, Hotkey, x502 y349 w160 h35 , 
Gui, Add, Hotkey, x502 y389 w160 h35 , 
Gui, Add, Hotkey, x502 y429 w160 h35 , 
; Repeat
Gui, Add, GroupBox, x670 y9 w30 h465 , Repeat
Gui, Add, Radio, x680 y29 w18 h35 ,
Gui, Add, Radio, x680 y69 w18 h35 , 
Gui, Add, Radio, x680 y109 w18 h35 ,
Gui, Add, Radio, x680 y149 w18 h35 ,
Gui, Add, Radio, x680 y189 w18 h35 ,
Gui, Add, Radio, x680 y229 w18 h35 ,
Gui, Add, Radio, x680 y269 w18 h35 ,
Gui, Add, Radio, x680 y309 w18 h35 ,
Gui, Add, Radio, x680 y349 w18 h35 ,
Gui, Add, Radio, x680 y389 w18 h35 ,
Gui, Add, Radio, x680 y429 w18 h35 ,
; Bottom
Gui, Add, Text, x12 y481 w70 h20 , Toggle repeat
Gui, Add, Hotkey, x82 y479 w100 h20 , 
Gui, Add, Edit, x189 y479 w60 h20 , Edit
Gui, Add, Text, x255 y481 w20 h20 , ms
Gui, Add, Text, x292 y481 w60 h20 , Auto-Enter
Gui, Add, CheckBox, x347 y479 w60 h20 ,
Gui, Add, Text, x600 y499 w90 h35 , Version control
Gui, Add, Button, x12 y509 w50 h20 , Save

; Generated using SmartGUI Creator 4.0
Gui, Show, x120 y106 h535 w720, New GUI Window
Return

GuiClose:
ExitApp